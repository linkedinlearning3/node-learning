const fs = require('fs');
const http = require("http");
const url = require('url');
//import { upperCase, localeUpperCase } from "upper-case";
const uc = require('upper-case')

http.createServer(function (req, res) {
    const  q = url.parse(req.url, true);
    const fileName = "." + q.pathname;

    fs.readFile(fileName, function(err, data) {
        if (err) {
            res.writeHead(400, {'Content-Type': 'text/html'})
            return res.end('Error when reading file. Details: ' + err.stack)
        } else {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(uc.upperCase("string"));
            res.write(data);
            return res.end();
        }
    });
}).listen(8080);
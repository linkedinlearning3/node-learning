const http = require('http');
const date = require('./myfirstmodule');
const url = require('url');

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write("Current date is: " + date.myDateTime() + "\n");
    res.write(req.url + "\n");

    const q = url.parse(req.url, true).query;
    const txt = q.year + " " + q.month;
    res.end(txt);
    //res.end('Hello World!');
}).listen(8080);